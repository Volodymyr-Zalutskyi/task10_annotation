package task_1;

import java.lang.reflect.Field;

public class ReflectionMenu {
    public static void main(String[] args) {
        Class clazz = MyClass.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Table.class)) {
                Table table = field.getAnnotation(Table.class);
                String name = table.name();
                int age = table.age();

                System.out.println("field :" + field.getName());

                System.out.println("name :" + name);
                System.out.println("age :" + age);
            }
        }

    }
}
