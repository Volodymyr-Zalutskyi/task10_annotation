package task_3;

public class ReturnType {

    public int setValue() {
        System.out.println("setValue");
        return 24;
    }

    public String getValue() {
        System.out.println("getValue");
        return "getValue";
    }

    public void setMenuValues(int value1, String value2) {
        System.out.println("setMenuValues");
    }
}
