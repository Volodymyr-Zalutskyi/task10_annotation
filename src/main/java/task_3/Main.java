package task_3;

import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        try {
            Class clazz = ReturnType.class;
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                if (method.getName().equals("setValue")
                        || (method.getName().equals("getValue"))
                        || (method.getName().equals("setMenuValues"))) {

                    Class returnType = method.getReturnType();
                    System.out.println("\nMethod Name : " + method.getName());
                    System.out.println("Return Type Details: " + returnType.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
