package task_2;

import task_2.Display;
import task_2.MyAnnotation;

import java.lang.annotation.Annotation;

/**
 * We can access the annotation of a class, method or field at
 * runtime.
 * <p>
 * Class Annotation example:
 */
public class ReflectionDemo1 {
    public static void main(String[] args) {
        Class<Display> classObj = Display.class;
        /*
         * Returns: annotations present on this element
         */
        Annotation[] annotationArray = classObj.getAnnotations();
        for (Annotation annotation : annotationArray) {
            if (annotation instanceof MyAnnotation) {
                MyAnnotation myAnnotation = (MyAnnotation) annotation;
                System.out.println("name : " + myAnnotation.name());
                System.out.println("age : " + myAnnotation.value());
            }
        }
    }
}