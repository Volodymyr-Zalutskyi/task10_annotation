package task_2;

import task_2.Display;
import task_2.MyAnnotation;

/**
 * We can access the annotation of a class, method or field at
 * runtime.
 * <p>
 * Class Annotation example:
 */
public class ReflectionDemo2 {
    public static void main(String[] args) {
        Class<Display> classObj = Display.class;
        /*
         * Returns: this elements annotation for the specified
         * annotation type if present on this element, else null
         */
        MyAnnotation myAnnotation = classObj.getAnnotation(MyAnnotation.class);
        System.out.println("name : " + myAnnotation.name());
        System.out.println("age : " + myAnnotation.value());
    }
}
