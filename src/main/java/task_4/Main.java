package task_4;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) {
        ChangeValue instance = new ChangeValue();
        System.out.println(instance.getValue());
        try {

            Class  clazz = instance.getClass();
            Field field = clazz.getDeclaredField("value");
            field.setAccessible(true);
            field.set(instance,  100);

            System.out.println(instance.getValue());

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
