package task_4;

public class ChangeValue {
    private int value = 40;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
