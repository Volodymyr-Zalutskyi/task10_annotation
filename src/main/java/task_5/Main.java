package task_5;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
    ClassInfo classInfo = new ClassInfo();
        informationAboutClass(classInfo);

    }
    static void informationAboutClass(Object object) {
        try {
            Class clazz = object.getClass();
            System.out.println("The name of class is : " + clazz.getSimpleName());
            System.out.println("The name of class package is : " + clazz.getName());

            Constructor constructor = clazz.getConstructor();
            System.out.println("The name of constructor is : " + constructor.getName());

            Method[] methods = clazz.getMethods();
            for (Method method: methods) {
                System.out.println("method  : " + method.getName());
            }

            Field[] fields = clazz.getDeclaredFields();
            for (Field field: fields) {
                System.out.println("field  : " + field.getName());
            }
            System.out.println();
            fields[0].setAccessible(true);
            if(fields[0].getType() == String.class) {
                fields[0].set(object, "one hundred");
                System.out.println(object);
            }
            System.out.println();
            Method methodCall = clazz.getDeclaredMethod("privateMethod");
            methodCall.setAccessible(true);
            methodCall.invoke(object);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
