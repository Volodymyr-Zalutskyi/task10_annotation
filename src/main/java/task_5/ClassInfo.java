package task_5;

public class ClassInfo {
    String value1;
    String value2;

    private void privateMethod() {
        System.out.println("Value private object");
    }

    public String getValue1() {
        return this.value1;
    }

    public void setValue1(String value) {
        this.value1 = value;
    }

    public String getValue2() {
        return this.value2;
    }

    public String setValue2(String value) {
        this.value2 = value;
        return this.value2;
    }

    @Override
    public String toString() {
        return "ClassInfo{" +
                "value1='" + value1 + '\'' +
                ", value2='" + value2 + '\'' +
                '}';
    }
}
